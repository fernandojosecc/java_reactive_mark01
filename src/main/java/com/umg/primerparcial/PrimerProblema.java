package com.umg.primerparcial;

import dto.Persona;
import rx.Observable;
import rx.functions.Func2;

import java.util.ArrayList;
import java.util.List;

public class PrimerProblema {
    public static void main(String[] args) {

        List<Persona> personas = new ArrayList<>();
        personas.add(new Persona(1, 12,"Adriana"));
        personas.add(new Persona(2, 15,"Pablo"));
        personas.add(new Persona(3, 8,"Pedro"));
        personas.add(new Persona(4, 19,"Ana"));
        personas.add(new Persona(5, 41,"Isaac"));


        Observable miOBSERVABLE =
                Observable
                        .from(personas.toArray())
                        .map((result)->{
            Persona persona = (Persona) result;
            return persona.getEdad();
        })
                        .reduce(
                                new Func2<Integer, Integer, Integer>() {

                                    @Override
                                    public Integer call(Integer anterior, Integer actual) {
                                        if (anterior > actual) {
                                            return anterior;
                                        } else {
                                            return actual;
                                        }
                                    }
                                }
                        );
                        miOBSERVABLE.subscribe((mayor)->{
                            System.out.println("La mayor edad es: "+mayor);
                        });
        }
    }

