package com.umg.primerparcial;
import dto.Consolas;
import rx.Observable;
import rx.functions.Func2;
import java.util.ArrayList;
import java.util.List;

public class SegundoProblema {
    public static void main(String[] args) {
        List<Consolas> consolas = new ArrayList<>();

        Consolas datoConsola = new Consolas(0," ");
        consolas.add(datoConsola);
        consolas.add(new Consolas(300, "ps4"));
        consolas.add(new Consolas(300, "gamecube"));
        consolas.add(new Consolas(200, "external disk"));
        consolas.add(new Consolas(800, "laptop"));
        consolas.add(new Consolas(230, "vr"));

        Observable miobservable =
                Observable
                        .from(consolas.toArray())
                        .map((result) -> {
                            Consolas consola = (Consolas)  result;
                            return consola.getPrecio();
                        })
                        .reduce(
                                new Func2<Integer, Integer, Integer>() {
                                    @Override
                                    public Integer call(Integer acumulador, Integer actual) {
                                        return acumulador + actual;
                                    }
                                }
                        );

        miobservable.subscribe((sumatoria) -> {
            System.out.println("La sumatoria total de consolas es: " + sumatoria);
        });

        Observable resultadoReduce = Observable.from(consolas)
                .map((result) -> {
                    Consolas consola = (Consolas)  result;
                    return consola.getPrecio();
                })
                .reduce(
                        new Func2<Integer, Integer, Integer>() {
                            @Override
                            public Integer call(Integer anterior, Integer actual) {
                                if(anterior>actual){
                                    return anterior;
                                }else{
                                    return actual;
                                }
                            }
                        }
                );

        resultadoReduce.subscribe((mayor) -> {
            System.out.println("El precio maximo que hay es: " + mayor);
        });
    }
}
