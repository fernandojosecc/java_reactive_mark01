package com.umg.primerparcial;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.observables.MathObservable;

public class TercerProblema {
    public static void main(String[] args) {
        Integer[] numbers = {2,5,6,8,10,35,2,10};

        Observable listado = Observable.from(numbers);
        MathObservable
                .from(listado)
                .averageInteger(listado)
                .subscribe((promedio) -> {
                    System.out.println("El promedio de los numeros es: " + promedio);
                });

        Observable
                .from(numbers)
                .filter(
                        new Func1<Integer, Boolean>() {
                            @Override
                            public Boolean call(Integer actual) {
                                return actual>=10;
                            }
                        }
                )
                .subscribe((valor)->{
                    System.out.println("Numero mayor o igual a 10 : "+valor);
                });

        Observable sumatoriaValores = Observable.from(numbers)
                        .reduce(
                                new Func2<Integer, Integer, Integer>() {
                                    @Override
                                    public Integer call(Integer acumulador, Integer actual) {
                                        Integer resultadoAcumulacion = acumulador + actual;
                                        return resultadoAcumulacion;
                                    }
                                }
                        );

        sumatoriaValores.subscribe((sumatoria) -> {
            System.out.println("La sumatoria total de los valores es: " + sumatoria);
        });
    }
    }
